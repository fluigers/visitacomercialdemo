function displayFields(form,customHTML){ 
	
	form.setShowDisabledFields(true);
	var nrAtividade = getValue("WKNumState");
	var matUsuario = getValue("WKUser");

	customHTML.append("<script>");
	customHTML.append("$(document).ready(function(){ ");
	
	var INICIO= 0;
	var APROVACAO_GAR = 10;
	var CANCELAR_VISITA_GDM = 14;		
	var PREENCHER_RELATORIO_VISITA_EAR = 69;
	var FOLLOW_UP = 62;
	var aprovarRelatorio = 47;


	
	var campos_PROSPECT = ['nome_prospect', 'segmento_prospect', 'sistema_atual_prospect', 'necessidades_prospect',
	'cnpj_prospect', 'numfunc_prospect', 'fatura_prospect', 'telefone_prospect', 'obs_prospect']
	var campos_GAR = ['aprovacao_gar','nome_ear', 'id_ear', 'obs_aprovado_gar', 'motivo_reprovado_gar']
	var campos_alteracoes_ear_data =['alteracoes_ear_data']
	var campos_DATA_VISITA = ['data_visita_prospect']
	var campos_RELATORIO_VISITA = ["oportunidade_relatorio", "segmento_relatorio", "porte_empresa_relatorio", "ti_relatorio_select",
	"apresentacao_ear","empresa_filial_relatorio", "atividade_empresa_relatorio", 
	"produtos_principais_ear", "nome_contato_relatorio", "cargo_contato_relatorio", "sistema_especialista_relatorio", 
	"fatores_relatorio", "concorrentes_relatorio", "perspectiva_inicio_relatorio", "solucao_ear_select", 
	"publico_relatorio", "assunto_apresentacao_relatorio", "pontos_apresentacao_relatorio", "sistema_atual_relatorio",
	"fatura_prospect_relatorio", "budget_investimento_relatorio"]
	var campos_FALLOW_UP = ['FOLLOW_UP_QUESTAO_1', 'FOLLOW_UP_QUESTAO_2', 'FOLLOW_UP_QUESTAO_3', 'FOLLOW_UP_QUESTAO_4', 'FOLLOW_UP_QUESTAO_5','FOLLOW_UP_QUESTAO_6','FOLLOW_UP_QUESTAO_7','novo_contato_quando','novo_contato_qual','obs_follow_up']
	var campos_FollowUp_NovoContato = ['novo_contato_quando','novo_contato_qual']
	
	
		
	// começo das atividades
	if (nrAtividade != INICIO && nrAtividade != 4){
		customHTML.append('$("#btn-anexar").hide();')
	};

	if (nrAtividade == INICIO || nrAtividade == 4) {
		esconderCampo('#panel_gar');
		esconderCampo('#RELATORIO_VISITA_TAB');
		esconderCampo('#FOLLOW_UP_TAB');
		customHTML.append('$("#btn-anexar").show();');
		customHTML.append("$('#info_dataVisita').show().css('position','relative');");	
		customHTML.append('$("#data_visita_prospect").attr("readonly",true);');		

	};

	if (nrAtividade == APROVACAO_GAR){ 			
		esconderCampo('#RELATORIO_VISITA_TAB');
		esconderCampo('#FOLLOW_UP_TAB');		
		esconderCampo('#div_aprovado_gar');
		esconderCampo('#div_reprovado_gar');
		desabilitarCampos(campos_PROSPECT);
		if ( form.getFormMode() == "MOD" ){
			var nome_gar = getNomeResponsavel();
			form.setValue("nome_GAR",nome_gar);	
			definirPanelInicial('#panel_gar');
		};
		
	};

	if(nrAtividade == CANCELAR_VISITA_GDM){
		desabilitarCampos(campos_GAR);			
		esconderCampo('#panel_data_visita');
		esconderCampo('#RELATORIO_VISITA_TAB');
		esconderCampo('#FOLLOW_UP_TAB');
		
	};

	if(nrAtividade == PREENCHER_RELATORIO_VISITA_EAR){
		habilitarCampos(campos_alteracoes_ear_data);
		definirTabInicial("#tabSetores", "#RELATORIO_VISITA");
		desabilitarCampos(campos_PROSPECT);
		desabilitarCampos(campos_DATA_VISITA);
		desabilitarCampos(campos_GAR);
		esconderCampo('#FOLLOW_UP_TAB');
		if ( form.getFormMode() == "MOD" ){
					definirPanelInicial('#panel_relatorio_visita');
				};		
				
	};

	if(nrAtividade == FOLLOW_UP){
		definirTabInicial("#tabSetores", "#FOLLOW_UP");
		desabilitarCampos(campos_PROSPECT);
		desabilitarCampos(campos_DATA_VISITA);
		desabilitarCampos(campos_GAR);
		desabilitarCampos(campos_RELATORIO_VISITA);
	};

	customHTML.append("});");
	customHTML.append("</script>");

	function esconderCampo(seletorCampo){
		customHTML.append("$('"+seletorCampo+"').hide();");
	}

	function mostrarCampo(seletorCampo){
		customHTML.append("$('"+seletorCampo+"').show();");
	}

	
	function desabilitarCampos(campos){
		campos.forEach(function(campo){ 
			form.setEnabled(campo, false); 
		});		
	}
	
	function habilitarCampos(campos){
		campos.forEach(function(campo){ 
			form.setEnabled(campo, true); 
		});		
	}

	function definirTabInicial(grupoTabs, idTab){
		customHTML.append('$("'+grupoTabs+' a[href='+"'"+idTab+"']"+'").tab("show");');
	}

	function definirPanelInicial(idLinkPanel){
		customHTML.append("setTimeout(function(){FLUIGC.utilities.scrollTo('"+idLinkPanel+"', 600);}, 1000);");
	}

	function getNomeResponsavel(){
		var c1 = DatasetFactory.createConstraint("colleaguePK.colleagueId", matUsuario, matUsuario, ConstraintType.MUST);
		var filtro = new Array(c1);
		var dataset = DatasetFactory.getDataset("colleague", null, filtro, null);
		return dataset.getValue(0,"colleagueName");
	}

	function collapseShow(seletorPanel){
		customHTML.append("$('"+seletorPanel+"').addClass('in');");
	}

	function collapseHide(seletorPanel){
		customHTML.append("$('"+seletorPanel+"').removeClass('in');");
	}
	
}
