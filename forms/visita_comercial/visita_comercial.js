function eventoTrocaAbaSetor(){
	$('#tabSetores a').click(function(e) {
    	e.preventDefault();
    	$(this).tab('show');
	});
}


function EventoMostrarCamposAprovacaoGar(){
$(".aprovacao_gar").change(function(){
		mostrarCamposAprovacaoGar();
});
		
};


function mostrarCamposAprovacaoGar(){
	
		if($('#aprovado_gar').prop('checked')){
			$('#div_aprovado_gar').show();
			$('#div_reprovado_gar').hide();
			$('#div_reprovado_gar :input').val('');
console.log('if');
		}else if($('#reprovado_gar').prop('checked')) {
			$('#div_aprovado_gar').hide();
			$('#div_reprovado_gar').show();
			$('#div_aprovado_gar :input').val('');
			$('span[data-role="remove"]').click();
console.log('elseif');
		}else{
			$('#div_reprovado_gar').hide();
			$('#div_aprovado_gar').hide();
console.log('else');	
			}		
};


function EventomostrarCamposFollowup7(){
	$(".followUP_7").change(function(){
		mostrarCamposFollowup7();
	});		
};

function mostrarCamposFollowup7(){
	if($('#radFOLLOW_UP_QUESTAO_7_sim').prop('checked')){
			$('#div_followUp_7').show();
		}else if($('#radFOLLOW_UP_QUESTAO_7_nao').prop('checked')){
			$('#div_followUp_7').hide();
			$('#div_followUp_7 :input').val('');
		}

}

function EventomostrarDivMotivoCancelamentoVisita(){
	$("#visitaCancelada").change(function(){
		mostrarDivMotivoCancelamentoVisita();
	});
};

function mostrarDivMotivoCancelamentoVisita(){
	if($("#visitaCancelada").prop('checked')){
			$("#div_motico_visita_cancelada").show();
			$('input[name="alteracoes_ear_data"]').prop('checked', false);
		}else{
			$("#div_motico_visita_cancelada").hide();
		}
	};

function EventoCollapseShowRelatorio(){
 $(".alteracoes_ear_data").change(function(){
	collapseShowRelatorio();
	});
};

function collapseShowRelatorio(){
		if($('#nao_alteracoes_ear_data').prop('checked')){
			$('#collapse_infoEmpresa, #collapse_contatoEmpresa, #collapse_sistema, #collapse_apresentacao').addClass('in');
		}
		if($('#sim_alteracoes_ear_data').prop('checked')){
			$('#collapse_infoEmpresa, #collapse_contatoEmpresa, #collapse_sistema, #collapse_apresentacao').removeClass('in');
		}
}

function gerarCalendario(){
	FLUIGC.calendar('.calendar', {
			pickDate : true,
			pickTime : false,
		    minDate: new Date()
	});
	$(".calendar [readonly]").siblings().unbind( "click" );
}

function eventoAnexo(){
	$("#btn-anexar").click(function(){
		parent.frames.document.getElementById("ecm-navigation-inputFile-clone").click();
	})
}


function removerIdEarComZoom(){
	$(document).on('click', 'span[data-role="remove"]', function(){
		$('#id_ear').val('');
	});
}

function limparZoomAutomaticamente(){
	$($("#nome_ear").siblings()[1]).click(function(){
		$('span[data-role="remove"]').click();
	});
}

function setSelectedZoomItem(selectedItem) {
	if(selectedItem.inputName == "nome_ear") {	
		$("#id_ear").val(selectedItem.USER_CODE);		
	}
}

var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

function anexoMobile(){
	if( isMobile.any() ){
		$("#btn-anexar").remove();
		$("#div-anexar").append("<p><label class=\"control-label\">N\u00E3o esque\u00E7a de anexar a ficha de visita <\/label><\/p>");
	}
}

$( document ).ready(function() {	
	EventoMostrarCamposAprovacaoGar();
	mostrarCamposAprovacaoGar();
	EventoCollapseShowRelatorio();
	collapseShowRelatorio();
	EventomostrarCamposFollowup7();	
	mostrarCamposFollowup7();
	EventomostrarDivMotivoCancelamentoVisita();
	mostrarDivMotivoCancelamentoVisita();	
	eventoTrocaAbaSetor();		
	gerarCalendario();
	eventoAnexo();
	anexoMobile();
	removerIdEarComZoom();
	setTimeout(function(){
		limparZoomAutomaticamente();
	}, 1000);	
});