// **************************************************************************
// Função para consultar os Tipos de Audiencia
// **************************************************************************
// Data: 19/07/2015
// Autor: Igor Rodrigues TOTVS RS
// **************************************************************************
// Parâmetros:
// --------------------------------------------------------------------------
// fields : campos (Não utilizado)
// constraints : restrições (Não Utilizado)
// sortFields : campos de ordenação (não utilizado)
// **************************************************************************
// Faz consulta nas tabelas e traz somente o NOME E ID DOS USUARIOS DO FLUIG

function createDataset(fields, constraints, sortFields) {
	// 	Criando a estrutura do dataset de retorno
 
	var jndiName = "java:/jdbc/FluigDSRO";
	
	
	try
	{
		if(constraints == null || constraints[0] == undefined ){ 
		      var sql = "select u.FULL_NAME,ut.USER_CODE from FDN_USER u INNER JOIN FDN_USERTENANT ut ON u.USER_ID = ut.USER_ID ORDER BY FULL_NAME";	 
		    }else{
		      var sql = "select u.FULL_NAME,ut.USER_CODE from FDN_USER u INNER JOIN FDN_USERTENANT ut ON u.USER_ID = ut.USER_ID WHERE u.FULL_NAME LIKE '%"+ constraints[0].initialValue +"%' ORDER BY FULL_NAME";
		    }
		    
			log.info("FluigDS =>"+sql);

		var dsBusca = DatasetFactory.getDataset("dsSQL2", new Array(sql, jndiName), constraints, null);
	
	}
	catch (e) {
		throw e.message;
	}
  
	return dsBusca;
}