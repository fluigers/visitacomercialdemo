function afterTaskCreate(colleagueId){
	var nrAtividade 	= getValue("WKNumState");
    var nrSolicitacao 	= getValue("WKNumProces");
    var nrProxAtividade = getValue("WKNextState");
    var numEmpresa = getValue("WKCompany");

         

    
    var INICIO = 0;	
	var PREENCHER_RELATORIO_VISITA_EAR = 69;
    var APROVACAO_GAR = 10;

    if (nrAtividade == INICIO || nrAtividade == 4) {
        var dataSolicitacao = new Date();        
        var obj = hAPI.calculateDeadLineHours(dataSolicitacao, horarioEmSegundos(dataSolicitacao), 3, "Default");
        var data = obj[0];
        var segundos = obj[1];
        
        hAPI.setDueDate(nrSolicitacao,0,colleagueId, data, segundos);

    }
	
	if (nrProxAtividade == PREENCHER_RELATORIO_VISITA_EAR) { 
        
   
       var dataVisita = hAPI.getCardValue("data_visita_prospect");    	
    	var arrayData =  dataVisita.split("/", 3);
    	var dia = arrayData[0];
    	var mes = arrayData[1] -1;
    	var ano = arrayData[2];  
    	
    	
    	var dataPrazo = new Date();
    	dataPrazo.setDate(dia);
    	dataPrazo.setMonth(mes);
    	dataPrazo.setFullYear(ano);
    	
    		
        var obj = hAPI.calculateDeadLineHours(dataPrazo, 64800, 16, "Default");
 		var dt = obj[0];
		var segundos = obj[1];

		
	   hAPI.setDueDate(nrSolicitacao,0,colleagueId, dt, segundos);
	
    }

	
	function horarioEmSegundos(data){
	 	var arrayHorario = data.toTimeString().split(' ')[0].split(':');
        var horas = arrayHorario[0];
        var minutos = arrayHorario[1];
        var segundos = arrayHorario[2];
        return (+horas) * 60 * 60 + (+minutos) * 60 + (+segundos); 
	}
}