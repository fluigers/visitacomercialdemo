function afterTaskComplete(colleagueId,nextSequenceId,userList){
	var nrAtividade 	= getValue("WKNumState");
    var nrSolicitacao 	= getValue("WKNumProces");
    var comentariosUsuario = getValue("WKUserComment");
    
    var INICIO = 0 ;
	var APROVACAO_GAR = 10;
	var nomeEAR =  hAPI.getCardValue("nome_ear");
	var aprovacaoGAR = hAPI.getCardValue("aprovacao_gar"); 
	var dataVisita = hAPI.getCardValue("data_visita_prospect");

	
	if (nrAtividade == INICIO || nrAtividade == 4){

		var comentario = "Foi definida provável Data  da Visita: " + dataVisita + "<br/>" + comentariosUsuario;
		hAPI.setTaskComments(colleagueId, nrSolicitacao,  0, comentario)
	}

	if (nrAtividade == APROVACAO_GAR) { 	

		if (aprovacaoGAR == "aprovado"){		
		var comentario = "Foi definido EAR: " + nomeEAR  + "<br/> Foi definida a Data da Visita: " + dataVisita + "<br/>" + comentariosUsuario;
		hAPI.setTaskComments(colleagueId, nrSolicitacao,  0, comentario)
		}
	}
}